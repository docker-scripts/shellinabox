shellinabox
===========

I need to give shell access to my Linux students and the most easy way
is by using shellinabox. They can also install Linux at home, but still
it is more fun when you use Linux in a multi-user environment. It is also
easier for me to assign and evaluate homeworks.


## Install

  - First install `ds` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the shellinabox scripts: `ds pull shellinabox`

  - Create a directory for the shellinabox container: `ds init shellinabox @shell1`

  - Fix the settings: `cd /var/ds/shell1/; vim settings.sh`

  - Build image, create the container and configure it: `ds make`

## Other commands

```
ds shell
ds stop
ds start
ds snapshot

ds help
```
